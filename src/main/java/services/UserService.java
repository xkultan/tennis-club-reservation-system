package services;

import models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Takes arguments from controller, and validate them and use repository to run query on database
 *
 * @author Peter Kultán
 */
@Service
@ComponentScan("repositories")
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * returns all users or by phone number or name if are presented
     *
     * @param phoneNumber optional, search parameter
     * @param name optional, search parameter
     * @return returns List of found users
     */
    @Transactional
    public List<User> getUser(String phoneNumber, String name) {

        if (phoneNumber == null && name == null) {
            return userRepository.findAll();
        }

        List<User> users = new ArrayList<>();
        if (phoneNumber != null && phoneNumber.length() > 0) {
            if (phoneNumber.charAt(0) == '+') {
                phoneNumber = '0' + phoneNumber.replace('+', '0');
            }
            users = userRepository.findAllById(Collections.singleton(phoneNumber));
        }
        if (name != null && name.length() > 0) {
            if (users.size() == 0) {
                users = userRepository.findAllByName(name);
            } else {
                users.retainAll(userRepository.findAllByName(name));
            }
        }


        return users;
    }

    /**
     * add new user to database
     *
     * @param user user to be added to database
     */
    public void addNewUser(User user) {
        userRepository.save(user);
    }

    /**
     * remove user by its primary key - phone number, if user with phoneNumber is not in database IllegalStateException is thrown
     *
     * @param phoneNumber of user to be deleted
     */
    public void deleteUser(String phoneNumber) {
        if (!userRepository.existsById((phoneNumber))) {
            throw new IllegalStateException("user with phone number " + phoneNumber + " does not exist");
        }
        userRepository.deleteById(phoneNumber);
    }

    /**
     * update users name
     *
     * @param phoneNumber primary key of user
     * @param name new name of user
     */
    @Transactional
    public void updateUser(String phoneNumber, String name) {
        User user = userRepository.findById(phoneNumber)
                .orElseThrow(() -> new IllegalStateException("user with phone number " + phoneNumber + " does not exist"));
        if (name != null && name.length() > 0) {
            user.setName(name);
        }
    }
}
