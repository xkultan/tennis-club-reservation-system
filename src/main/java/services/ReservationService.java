package services;

import models.Court;
import models.Reservation;
import models.ReservationType;
import models.Surface;
import models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import repositories.CourtRepository;
import repositories.ReservationRepository;
import repositories.UserRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Takes arguments from controller, and validate them and use repository to run query on database
 *
 * @author Peter Kultán
 */
@Service
@ComponentScan("repositories")
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final CourtRepository courtRepository;
    private final UserRepository userRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, CourtRepository courtRepository, UserRepository userRepository) {
        this.reservationRepository = reservationRepository;
        this.courtRepository = courtRepository;
        this.userRepository = userRepository;
    }

    /**
     * returns all reservations or by reservation id, phone number, court id, surface, hour price, reservatrion type,
     * starting time, duration or name if are presented
     *
     * @param reservationId optional, search parameter
     * @param phoneNumber optional, search parameter
     * @param courtId optional, search parameter
     * @param surface optional, search parameter
     * @param hourPrice optional, search parameter
     * @param reservationType optional, search parameter
     * @param startingTime optional, search parameter
     * @param duration optional, search parameter
     * @param name optional, search parameter
     * @return returns List of found reservations
     */
    public List<Reservation> getReservation(Long reservationId, String phoneNumber,
                                            Long courtId, Surface surface, Double hourPrice,
                                            ReservationType reservationType, LocalDateTime startingTime, Integer duration, String name) {

        Object[] args = {reservationId, phoneNumber, courtId, surface, hourPrice, reservationType, startingTime, duration};
        if (Collections.frequency(Arrays.asList(args), null) == 8) {
            return reservationRepository.findAll();
        }

        List<Reservation> reservations = new ArrayList<>();


        if (reservationId != null) {
            return reservationRepository.findAllById(Collections.singleton(reservationId));
        }
        if (phoneNumber != null && phoneNumber.length() > 0) {
            reservations = reservationRepository.findAllByPhoneNumber(phoneNumber);
        }

        if (courtId != null) {
            if (reservations.size() == 0) {
                reservations = reservationRepository.findAllByCourtId(courtId);
            } else {
                reservations.retainAll(reservationRepository.findAllByCourtId(courtId));
            }
        }

        if (surface != null) {
            for (Court court : courtRepository.findAllBySurface(surface)) {
                if (reservations.size() == 0) {
                    reservations = reservationRepository.findAllByCourtId(court.getId());
                } else {
                    reservations.retainAll(reservationRepository.findAllByCourtId(court.getId()));
                }
            }
        }

        if (hourPrice != null) {
            for (Court court : courtRepository.findAllByPrice(hourPrice)) {
                if (reservations.size() == 0) {
                    reservations = reservationRepository.findAllByCourtId(court.getId());
                } else {
                    reservations.retainAll(reservationRepository.findAllByCourtId(court.getId()));
                }
            }
        }

        if (reservationType != null) {
            if (reservations.size() == 0) {
                reservations = reservationRepository.findAllByType(reservationType);
            } else {
                reservations.retainAll(reservationRepository.findAllByType(reservationType));
            }
        }

        if (startingTime != null) {
            if (reservations.size() == 0) {
                reservations = reservationRepository.findAllByStartingTime(startingTime);
            } else {
                reservations.retainAll(reservationRepository.findAllByStartingTime(startingTime));
            }
        }

        if (duration != null) {
            if (reservations.size() == 0) {
                reservations = reservationRepository.findAllByDuration(duration);
            } else {
                reservations.retainAll(reservationRepository.findAllByDuration(duration));
            }
        }

        if (name != null && name.length() > 0) {
            for (User user : userRepository.findAllByName(name)) {
                if (reservations.size() == 0) {
                    reservations = reservationRepository.findAllByPhoneNumber(user.getPhoneNumber());
                } else {
                    reservations.retainAll(reservationRepository.findAllByPhoneNumber(user.getPhoneNumber()));
                }
            }
        }
        return reservations;
    }

    /**
     * add new reservation to database
     *
     * @param reservation to be added to database
     * @return price of newly created reservation
     */
    public Double addNewReservation(Reservation reservation) {
        verifyNewParameters(reservation);
        if (userRepository.findById(reservation.getPhoneNumber()).isEmpty()) {
            userRepository.save(new User(reservation.getPhoneNumber(), reservation.getUserName()));
        }
        reservationRepository.save(reservation);

        return calculatePrice(reservation);
    }

    /**
     * verify parameters of new reservation as if are all presented, if court with courtId exists
     *
     * @param reservation new reservation to be verified
     */
    private void verifyNewParameters(Reservation reservation) {
        Object[] args = {reservation.getPhoneNumber(), reservation.getCourtId(), reservation.getReservationType(),
                reservation.getStartTime(), reservation.getDuration(), reservation.getUserName()};
        if (Collections.frequency(Arrays.asList(args), null) >= 1) {
            throw new IllegalStateException("One of required param was not entered");
        }
        if (courtRepository.findById(reservation.getCourtId()).isEmpty()) {
            throw new IllegalStateException("Court with id " + reservation.getCourtId() + " does not exist");
        }
        if (!reservation.getStartTime().isAfter(LocalDateTime.now())) {
            throw new IllegalStateException("starting must be in future");
        }
        if (reservation.getDuration() < 1) {
            throw new IllegalStateException("duration must be greater than 0");
        }

        checkOverlapping(reservation);
    }

    /**
     * remove reservation based on its id and if it was only reservation of user, user is removed too, but if
     * reservation with id reservationId does not exist, IllegalStateException is thrown
     *
     * @param reservationId of reservation to be removed
     */
    public void deleteReservation(Long reservationId) {
        if (!reservationRepository.existsById(reservationId)) {
            throw new IllegalStateException("Reservation with id " + reservationId + " does not exists");
        }
        String phoneNumber = (reservationRepository.getById(reservationId).getPhoneNumber());
        if(reservationRepository.findAllByPhoneNumber(phoneNumber).isEmpty()){
            userRepository.deleteById(phoneNumber);
        }
        reservationRepository.deleteById(reservationId);
    }

    /**
     * Change reservation phone number, courtId, reservation type, starting time or duration
     *
     * @param reservationId primary key of reservation to be updated
     * @param phoneNumber optional, new phone number of reservation
     * @param courtId optional, new id of court
     * @param reservationType optional, new reservation type
     * @param startingTime optional, new starting time of reservation
     * @param duration optional, new duration of reservation
     * @return price of reservation
     */
    public Double updateReservation(Long reservationId, String phoneNumber, Long courtId, ReservationType reservationType, LocalDateTime startingTime, Integer duration) {
        Reservation reservation = reservationRepository
                .findById(reservationId).orElseThrow(() -> new IllegalStateException("Reservation with id " + reservationId + " does not exist"));
        if (phoneNumber != null && phoneNumber.length() > 0) {
            changePhoneNumber(reservation, phoneNumber);
        }

        if (courtId != null) {
            changeCourtId(reservation, courtId);
        }

        if (reservationType != null) {
            reservation.setReservationType(reservationType);
        }
        if (startingTime != null) {
            LocalDateTime oldStartingTime = reservation.getStartTime();
            reservation.setStartTime(startingTime);
            try {
                checkOverlapping(reservation);
            } catch (IllegalStateException ex) {
                reservation.setStartTime(oldStartingTime);
                throw ex;
            }
        }

        if (duration != null && duration > 0) {
            int oldDuration = reservation.getDuration();
            reservation.setDuration(duration);
            try {
                checkOverlapping(reservation);
            } catch (IllegalStateException ex) {
                reservation.setDuration(oldDuration);
                throw ex;
            }
        }
        return calculatePrice(reservation);
    }

    /**
     * Check if new reservation is  not overlapping with existing one court
     *
     * @param reservation to be checked
     */
    private void checkOverlapping(Reservation reservation) {
        List<Reservation> overlapping = reservationRepository.findAll().stream()
                .filter(x -> reservation.getStartTime().isBefore(x.getStartTime().plusHours(x.getDuration())) && x.getStartTime().isBefore(reservation.getStartTime().plusHours(reservation.getDuration())))
                .collect(Collectors.toList());
        if (overlapping.size() > 0) {
            throw new IllegalStateException("Reservation is overlapping with one or more reservations");
        }
    }

    /**
     * Check if court with courtId exists and if exists change reservation to it, but if does not exist
     * IllegalStateException is thrown
     *
     * @param reservation to be updated court id
     * @param courtId court id to be checked
     */
    private void changeCourtId(Reservation reservation, Long courtId) {
        courtRepository.findById(courtId).orElseThrow(() -> new IllegalStateException("Court with Id " + courtId + " does not exist"));
        reservation.setCourtId(courtId);
    }

    /**
     * change phone number of reservation
     *
     * @param reservation to be updated
     * @param phoneNumber new phone number
     */
    private void changePhoneNumber(Reservation reservation, String phoneNumber) {
        if (!reservation.getPhoneNumber().equals(phoneNumber)) {
            List<Reservation> reservationList = reservationRepository.findAllByPhoneNumber(phoneNumber);
            String userName = userRepository.findById(phoneNumber).get().getName();
            if (reservationList.size() == 1) {
                userRepository.deleteById(phoneNumber);
            }
            userRepository.save(new User(phoneNumber, userName));
            reservation.setPhoneNumber(phoneNumber);
        }
    }

    private Double calculatePrice(Reservation reservation) {
        double price = courtRepository.findById(reservation.getCourtId()).get().getHourPrice() * reservation.getDuration();

        return reservation.getReservationType() == ReservationType.SINGLES ? price : price * 1.5;
    }
}
