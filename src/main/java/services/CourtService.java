package services;

import models.Court;
import models.Surface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import repositories.CourtRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Takes arguments from controller, and validate them and use repository to run query on database
 *
 * @author Peter Kultán
 */
@Service
@ComponentScan("repositories")
public class CourtService {

    private final CourtRepository courtRepository;

    @Autowired
    public CourtService(CourtRepository courtRepository) {
        this.courtRepository = courtRepository;
    }

    /**
     * returns all courts or by id, surface or hour price if are presented
     *
     * @param id optional, search parameter
     * @param surface optional, search parameter
     * @param hourPrice optional, search parameter
     * @return returns List of found users
     */
    @Transactional
    public List<Court> getCourt(Long id, Surface surface, Double hourPrice) {
        List<Court> courts = new ArrayList<>();

        if (id != null) {
            courts = courtRepository.findAllById(Collections.singleton(id));
        }
        if (surface != null) {
            if (courts.size() == 0) {
                courts = courtRepository.findAllBySurface(surface);
            } else {
                courts.retainAll(courtRepository.findAllBySurface(surface));
            }
        }
        if (hourPrice != null) {
            if (courts.size() == 0) {
                courts = courtRepository.findAllByPrice(hourPrice);
            } else {
                courts.retainAll(courtRepository.findAllByPrice(hourPrice));
            }
        }
        return courts.size() == 0 ? courtRepository.findAll() : courts;
    }

    /**
     * add new court to database
     *
     * @param court to be added to database
     */
    public void addNewCourt(Court court) {
        courtRepository.save(court);
    }

    /**
     * remove court from database based on its primary key - id, when court with courtId is not in database
     * IllegalStateException is thrown
     *
     * @param courtId court to be removed
     */
    public void deleteCourt(Long courtId) {
        if (!courtRepository.existsById(courtId)) {
            throw new IllegalStateException("court with id " + courtId + " does not exist");
        }
        courtRepository.deleteById(courtId);
    }

    /**
     * update surface or hour price of court
     *
     * @param courtId of court to be updated
     * @param surface optional, new surface of court
     * @param hourPrice optional, new hour price of court
     */
    @Transactional
    public void updateCourt(Long courtId, Surface surface, Double hourPrice) {
        Court court = courtRepository.findById(courtId).orElseThrow(() -> new IllegalStateException("court with id " + courtId + " does not exist"));
        if (surface != null) {
            court.setSurface(surface);
        }
        if (hourPrice != null) {
            court.setHourPrice(hourPrice);
        }
    }
}
