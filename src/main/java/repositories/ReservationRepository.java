package repositories;

import models.Reservation;
import models.ReservationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Used to run query on database
 *
 * @author Peter Kultán
 */
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query("SELECT r FROM Reservation r WHERE r.phoneNumber = ?1")
    List<Reservation> findAllByPhoneNumber(String phoneNumber);

    @Query("SELECT r FROM Reservation r WHERE r.courtId = ?1")
    List<Reservation> findAllByCourtId(Long courtId);

    @Query("SELECT r FROM Reservation  r WHERE r.reservationType = ?1")
    List<Reservation> findAllByType(ReservationType reservationType);

    @Query("SELECT r FROM Reservation r WHERE r.startTime = ?1")
    List<Reservation> findAllByStartingTime(LocalDateTime startingTime);

    @Query("SELECT r FROM Reservation r WHERE r.duration = ?1")
    List<Reservation> findAllByDuration(Integer duration);
}
