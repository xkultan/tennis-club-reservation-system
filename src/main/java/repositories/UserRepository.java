package repositories;

import models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Used to run query on database
 *
 * @author Peter Kultán
 */
public interface UserRepository extends JpaRepository<User, String> {

    @Query("SELECT u FROM User u where u.name = ?1")
    List<User> findAllByName(String name);
}
