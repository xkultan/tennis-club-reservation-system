package repositories;

import models.Court;
import models.Surface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Used to run query on database
 *
 * @author Peter Kultán
 */
@Repository
public interface CourtRepository extends JpaRepository<Court, Long> {

    @Query("SELECT c FROM Court c where c.surface = ?1")
    List<Court> findAllBySurface(Surface surface);

    @Query("SELECT c FROM Court c WHERE c.price = ?1")
    List<Court> findAllByPrice(Double hourPrice);
}
