package controllers;

import models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import services.UserService;

import java.util.List;

/**
 * Controller that is used for parsing arguments from rest api to service from Get, Post, Delete and Put endpoints.
 *
 * @author Peter Kultán
 */
@RestController
@ComponentScan("services")
@RequestMapping(path = "api/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUser(@RequestParam(required = false) String phoneNumber,
                              @RequestParam(required = false) String name) {
        return userService.getUser(phoneNumber, name);
    }

    @PostMapping
    public void addNewUser(@RequestBody User user) {
        userService.addNewUser(user);
    }

    @DeleteMapping(path = "{phoneNumber}")
    public void deleteUser(@PathVariable("phoneNumber") String phoneNumber) {
        userService.deleteUser(phoneNumber);
    }

    @PutMapping(path = "{phoneNumber}")
    public void updateUser(@PathVariable("phoneNumber") String phoneNumber,
                           @RequestParam(required = false) String name) {
        userService.updateUser(phoneNumber, name);
    }
}
