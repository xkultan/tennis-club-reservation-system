package controllers;

import com.sun.xml.bind.v2.TODO;
import models.Reservation;
import models.ReservationType;
import models.Surface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import services.ReservationService;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Controller that is used for parsing arguments from rest api to service from Get, Post, Delete and Put endpoints.
 *
 * @author Peter Kultán
 */
@RestController
@ComponentScan("services")
@RequestMapping(path = "api/reservation")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping
    List<Reservation> getReservation(@RequestParam(required = false) Long reservationId,
                                     @RequestParam(required = false) String phoneNumber,
                                     @RequestParam(required = false) Long courtId,
                                     @RequestParam(required = false) Surface surface,
                                     @RequestParam(required = false) Double hourPrice,
                                     @RequestParam(required = false) ReservationType reservationType,
                                     @RequestParam(required = false) LocalDateTime startingTime,
                                     @RequestParam(required = false) Integer duration,
                                     @RequestParam(required = false) String name) {
        return reservationService.getReservation(reservationId, phoneNumber, courtId,
                                          surface, hourPrice, reservationType, startingTime, duration, name);
    }

    @PostMapping
    Double addNewReservation(@RequestBody Reservation reservation) {
        return reservationService.addNewReservation(reservation);
    }

    @DeleteMapping(path="{reservationId}")
    void deleteReservation(@PathVariable("reservationId") Long reservationId) {
        reservationService.deleteReservation(reservationId);
    }

    @PutMapping(path = "{reservationId}")
    Double UpdateReservation(@PathVariable Long reservationId,
                           @RequestParam(required = false) String phoneNumber,
                           @RequestParam(required = false) Long courtId,
                           @RequestParam(required = false) ReservationType reservationType,
                           @RequestParam(required = false) LocalDateTime startingTime,
                           @RequestParam(required = false) Integer duration) {
        return reservationService.updateReservation(reservationId, phoneNumber, courtId, reservationType, startingTime, duration);
    }
}
