package controllers;

import models.Surface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;
import services.CourtService;
import models.Court;

import java.util.List;

/**
 * Controller that is used for parsing arguments from rest api to service from Get, Post, Delete and Put endpoints.
 *
 * @author Peter Kultán
 */
@RestController
@ComponentScan("services")
@RequestMapping(path = "api/court")
public class CourtController {

    private final CourtService courtService;

    @Autowired
    public CourtController(CourtService courtService) {
        this.courtService = courtService;
    }

    @GetMapping
    public List<Court> getCourt(@RequestParam(required = false) Long courtId,
                                @RequestParam(required = false) Surface surface,
                                @RequestParam(required = false) Double hourPrice) {
        return courtService.getCourt(courtId, surface, hourPrice);
    }

    @PostMapping
    public void addNewCourt(@RequestBody Court court) {
        courtService.addNewCourt(court);
    }

    @DeleteMapping(path = "{courtId}")
    public void deleteCourt(@PathVariable("courtId") Long courtId) {
        courtService.deleteCourt(courtId);
    }

    @PutMapping(path = "{courtId}")
    public void updateCourt(@PathVariable("courtId") Long courtId,
                            @RequestParam(required = false) Surface surface,
                            @RequestParam(required = false) Double hourPrice) {
        courtService.updateCourt(courtId, surface, hourPrice);
    }
}
