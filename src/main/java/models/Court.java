package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Represent Table of Courts. Primary key represents here courtId. Surface and hour price is saved with it.
 *
 * @author Peter Kultán
 */
@Entity
@Table
public class Court {
    @Id
    @SequenceGenerator(
            name = "court_sequence",
            sequenceName = "court_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "court_sequence"
    )
    private Long id;
    private Surface surface;
    private Double price;

    public Court() {
    }

    public Court(Long id, Surface surface, Double hourPrice) {
        this.id = id;
        this.surface = surface;
        this.price = hourPrice;
    }

    public Court(Surface surface, Double hourPrice) {
        this.surface = surface;
        this.price = hourPrice;
    }

    public Long getId() {
        return id;
    }

    public Surface getSurface() {
        return surface;
    }

    public Double getHourPrice() {
        return price;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSurface(Surface surface) {
        this.surface = surface;
    }

    public void setHourPrice(Double hourPrice) {
        this.price = hourPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Court court = (Court) o;
        return Objects.equals(id, court.id) && surface == court.surface && Objects.equals(price, court.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surface, price);
    }

    @Override
    public String toString() {
        return "Court{" +
                "id=" + id +
                ", surface='" + surface + '\'' +
                ", hourPrice='" + price + '\'' +
                '}';


    }
}
