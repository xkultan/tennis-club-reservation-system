package models;

/**
 * enum used to determine if reservation is doubles or singles.
 *
 * @author Peter Kultán
 */
public enum ReservationType {
    SINGLES,
    DOUBLES;

    @Override
    public String toString() {
        return "ReservationType{}";
    }
}
