package models;

/**
 * enum used to determine surface of court
 *
 * @author Peter Kultán
 */
public enum Surface {
    GRASS,
    CLAY,
    HARD,
    ARTIFICIAL_GRASS
}
