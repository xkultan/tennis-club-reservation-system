package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Represent table in database used to store users. Primary key is here phone number stored as string. when user is
 * created or phone number is being changed, it is verified for length. it is stored users name too.
 *
 * @author Peter Kultán
 */
@Entity
@Table
public class User {
    @Id
    private String phoneNumber;
    private String name;

    public User() {
    }

    public User(String phoneNumber, String name) {
        this.name = name;
        this.phoneNumber = formatPhoneNumber(phoneNumber);
    }

    private String formatPhoneNumber(String phoneNumber) {
        if (phoneNumber.charAt(0) == '+') {
            if (phoneNumber.length() != 13) {
                throw new IllegalStateException("Bad telephone number");
            }
            phoneNumber = phoneNumber.replace('+', '0');
            phoneNumber = '0' + phoneNumber;
        } else {
            if (phoneNumber.length() != 10 && phoneNumber.length() != 14) {
                throw new IllegalStateException("Bad telephone number");
            }
        }
        if (!Pattern.matches("[0-9]+", phoneNumber)) {
            throw new IllegalStateException("Bad telephone number");
        }
        return phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = formatPhoneNumber(phoneNumber);
    }

    @Override
    public String toString() {
        return "User{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(phoneNumber, user.phoneNumber) && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNumber, name);
    }
}
