package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Class represent reservation table with primary key id and foreign keys - phoneNumber (from user) and courtId (from
 * courts). When creating new reservation court with courtId must exist, but if user with entered phone number and name
 * does not exist is created. reservation type define if reservation is doubles or singles. Start time determines when
 * reservation start and duration for how much is court reserved from start. userName is not saved in database, it is
 * used only for creating new user when he does not exist.
 *
 * @author Peter Kultán
 */
@Entity
@Table
public class Reservation {


    @Id
    @SequenceGenerator(
            name = "reservation_sequence",
            sequenceName = "reservation_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "reservation_sequence"
    )
    private Long id;
    private String phoneNumber;
    private Long courtId;
    private ReservationType reservationType;
    private LocalDateTime startTime;
    private int duration;
    @Transient
    private String userName;

    private String formatPhoneNumber(String phoneNumber) {
        if(phoneNumber.charAt(0) == '+') {
            if(phoneNumber.length() != 13){
                throw new IllegalStateException("Bad telephone number");
            }
            phoneNumber = phoneNumber.replace('+', '0');
            phoneNumber = '0' + phoneNumber;
        }
        else {
            if(phoneNumber.length() != 10 && phoneNumber.length() != 14) {
                throw new IllegalStateException("Bad telephone number");
            }
        }
        if(!Pattern.matches("[0-9]+", phoneNumber)) {
            throw new IllegalStateException("Bad telephone number");
        }
        return phoneNumber;
    }

    public Reservation() {
    }

    public Reservation(Long id, String phoneNumber, Long courtId, ReservationType reservationType, LocalDateTime startTime, int duration) {
        this.id = id;
        this.phoneNumber = formatPhoneNumber(phoneNumber);
        this.courtId = courtId;
        this.reservationType = reservationType;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Reservation(String phoneNumber, Long courtId, ReservationType reservationType, LocalDateTime startTime, int duration) {
        this.phoneNumber = formatPhoneNumber(phoneNumber);
        this.courtId = courtId;
        this.reservationType = reservationType;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Reservation(String phoneNumber, Long courtId, ReservationType reservationType, LocalDateTime startTime, int duration, String userName) {
        this.phoneNumber = formatPhoneNumber(phoneNumber);
        this.courtId = courtId;
        this.reservationType = reservationType;
        this.startTime = startTime;
        this.duration = duration;
        this.userName = userName;
    }

    public Reservation(Long id, String phoneNumber, Long courtId, ReservationType reservationType, LocalDateTime startTime, int duration, String userName) {
        this.id = id;
        this.phoneNumber = formatPhoneNumber(phoneNumber);
        this.courtId = courtId;
        this.reservationType = reservationType;
        this.startTime = startTime;
        this.duration = duration;
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Long getCourtId() {
        return courtId;
    }

    public ReservationType getReservationType() {
        return reservationType;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public int getDuration() {
        return duration;
    }

    public String getUserName() {
        return userName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = formatPhoneNumber(phoneNumber);
    }

    public void setCourtId(Long courtId) {
        this.courtId = courtId;
    }

    public void setReservationType(ReservationType reservationType) {
        this.reservationType = reservationType;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return duration == that.duration && Objects.equals(id, that.id) && Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(courtId, that.courtId) && reservationType == that.reservationType && Objects.equals(startTime, that.startTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phoneNumber, courtId, reservationType, startTime, duration, userName);
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", courtId=" + courtId +
                ", reservationType=" + reservationType +
                ", startTime=" + startTime +
                ", duration=" + duration +
                ", userName='" + userName + '\'' +
                '}';
    }
}
