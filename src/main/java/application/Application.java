package application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Class that runs webApplication
 *
 * @author Peter Kultán
 */
@SpringBootApplication(scanBasePackages = {"controllers", "services"})
@EnableJpaRepositories("repositories")
@EntityScan("models")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
