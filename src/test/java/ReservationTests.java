import application.Application;
import models.Court;
import models.Reservation;
import models.ReservationType;
import models.Surface;
import models.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import repositories.CourtRepository;
import repositories.ReservationRepository;
import repositories.UserRepository;
import services.ReservationService;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application.properties")
public class ReservationTests {

    private final List<User> userList = List.of(new User("+420122054854", "Oliver"),
            new User("00420985425745", "Andrej"));
    private final List<Reservation> testData = List.of(new Reservation("+420122054854", null, ReservationType.SINGLES, LocalDateTime.of(2021, 12, 12, 18, 0), 2, "Oliver"),
            new Reservation("00420985425745", null, ReservationType.DOUBLES, LocalDateTime.of(2021, 12, 12, 20, 0), 4, "Andrej"));
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private CourtRepository courtRepository;
    @Autowired
    private UserRepository userRepository;
    private List<Court> courtList = List.of(new Court(Surface.GRASS, 4.25),
            new Court(Surface.CLAY, 5.25));

    @Before
    public void setUp() {
        reservationRepository.deleteAll();
        courtRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void addNewReservation() {
        courtRepository.saveAll(courtList);
        courtList = courtRepository.findAll();
        testData.get(0).setCourtId(courtList.get(0).getId());
        testData.get(1).setCourtId(courtList.get(1).getId());
        for (Reservation reservation : testData) {
            reservationService.addNewReservation(reservation);
        }
        assert (reservationRepository.findAll().equals(testData));
        assert (userRepository.findAll().equals(userList));
    }

    @Test
    public void addWithoutCourt() {
        for (Reservation reservation : testData) {
            try {
                reservationService.addNewReservation(reservation);
                assert (false);
            } catch (Exception ignored) {
            }
        }
    }

    @Test
    public void addWithExistingUser() {
        courtRepository.saveAll(courtList);
        courtList = courtRepository.findAll();
        testData.get(0).setCourtId(courtList.get(0).getId());
        testData.get(1).setCourtId(courtList.get(1).getId());
        userRepository.saveAll(userList);
        for (Reservation reservation : testData) {
            reservationService.addNewReservation(reservation);
        }
        assert (reservationRepository.findAll().equals(testData));
        assert (userRepository.findAll().equals(userList));
    }

    @Test
    public void getReservations() {
        courtRepository.saveAll(courtList);
        courtList = courtRepository.findAll();
        testData.get(0).setCourtId(courtList.get(0).getId());
        testData.get(1).setCourtId(courtList.get(1).getId());
        userRepository.saveAll(userList);
        reservationRepository.saveAll(testData);
        assert (reservationService.getReservation(null, null, null, null, null, null, null, null, null).equals(testData));
    }

    @Test
    public void getReservationById() {
        courtRepository.saveAll(courtList);
        courtList = courtRepository.findAll();
        testData.get(0).setCourtId(courtList.get(0).getId());
        testData.get(1).setCourtId(courtList.get(1).getId());
        userRepository.saveAll(userList);
        reservationRepository.saveAll(testData);
        for (Reservation reservation : testData) {
            assert (reservationService.getReservation(reservation.getId(), null, null, null, null, null, null, null, null).get(0).equals(reservation));
        }
    }
}
