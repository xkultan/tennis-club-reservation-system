import application.Application;
import models.Court;
import models.Surface;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import repositories.CourtRepository;
import services.CourtService;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application.properties")
public class CourtTests {

    private final List<Court> testData = List.of(new Court(Surface.CLAY, 2.50),
            new Court(Surface.ARTIFICIAL_GRASS, 5.00),
            new Court(Surface.GRASS, 9.25),
            new Court(Surface.HARD, 4.25),
            new Court(Surface.CLAY, 9.25),
            new Court(Surface.ARTIFICIAL_GRASS, 4.25),
            new Court(Surface.GRASS, 5.00),
            new Court(Surface.HARD, 2.50));
    @Autowired
    private CourtService courtService;
    @Autowired
    private CourtRepository courtRepository;

    @Test
    public void addNewCourtToDatabase() {
        for (Court court : testData) {
            courtService.addNewCourt(court);
        }
    }

    @Test
    public void getCourts() {
        courtRepository.deleteAll();
        courtRepository.saveAll(testData);

        assert (testData.equals(courtService.getCourt(null, null, null)));
    }

    @Test
    public void getCourtsBySurface() {
        courtRepository.deleteAll();
        courtRepository.saveAll(testData);
        for (Surface surface : Surface.values()) {
            List<Court> testList = testData.stream().filter(x -> x.getSurface().equals(surface)).collect(Collectors.toList());
            assert (testList.equals(courtService.getCourt(null, surface, null)));
        }
    }

    @Test
    public void getCourtsByPrice() {
        courtRepository.deleteAll();
        courtRepository.saveAll(testData);
        for (Court court : testData) {
            List<Court> testList = testData.stream().filter(x -> x.getHourPrice().equals(court.getHourPrice())).collect(Collectors.toList());
            assert (testList.equals(courtService.getCourt(null, null, court.getHourPrice())));
        }
    }
}
