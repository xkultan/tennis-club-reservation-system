import application.Application;
import models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import repositories.UserRepository;
import services.UserService;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application.properties")
public class UserTests {

    private static final List<String> secondNumbers = List.of("8527419630", "9638527419", "7418529637", "3216549871");
    private static final List<User> testData = List.of(new User("00123456789123", "Ján Novotný"),
            new User("00987654321987", "Oliver Polievka"),
            new User("0985456258", "Jaroslav Cyprich"),
            new User("0123456789", "Marek"));

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mvc;

    @Test
    public void createNewUser() {
        new User("+987654321052", "Peter");
        List<String> invalidPhoneNumbers = List.of("741", "8451216586555", "+85", "+998465198684513", "someText");
        for (String phoneNumber : invalidPhoneNumbers) {
            try {
                new User(phoneNumber, "Ján");
                assert (false);
            } catch (Exception ignored) {
            }
        }
    }

    @Test
    public void addNewUserToDatabase() {
        for (User user : testData) {
            userService.addNewUser(user);
        }
    }

    @Test
    public void getUsers() {
        userRepository.deleteAll();
        userRepository.saveAll(testData);

        List<User> usersFromDatabase = new ArrayList<>(testData);
        usersFromDatabase.retainAll(userService.getUser(null, null));

        assert (testData.equals(usersFromDatabase));
    }

    @Test
    public void getUsersByNumber() {
        userRepository.deleteAll();
        userRepository.saveAll(testData);

        for (User user : testData) {
            User userFromDatabase = userService.getUser(user.getPhoneNumber(), null).get(0);
            assert (userFromDatabase.equals(user));
        }
    }

    @Test
    public void getUserByName() {
        userRepository.deleteAll();
        userRepository.saveAll(testData);

        for (User user : testData) {
            List<User> usersFromDatabase = userService.getUser(null, user.getName());
            assert (usersFromDatabase.equals(List.of(user)));
        }
    }

    @Test
    public void twoSameNamesInDatabase() {
        userRepository.deleteAll();
        userRepository.saveAll(testData);

        for (int i = 0; i < testData.size(); i++) {
            userRepository.save(new User(secondNumbers.get(i), testData.get(i).getName()));
        }
        for (int i = 0; i < testData.size(); i++) {
            assert (userService.getUser(null, testData.get(i).getName())
                    .equals(List.of(testData.get(i), new User(secondNumbers.get(i), testData.get(i).getName()))));
        }
    }
}